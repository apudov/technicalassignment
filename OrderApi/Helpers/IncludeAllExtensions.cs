﻿
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace OrderApi.Helpers
{
    public static class IncludeAllExtensions
    {
        public static IQueryable<T> IncludeAll<T>(this IQueryable<T> queryable) where T : class
        {
            var type = typeof(T);
            var properties = type.GetProperties();

            foreach (var property in properties)
            {
                if (property.GetGetMethod().IsVirtual)
                {
                    queryable = queryable.Include(property.Name);
                }
            }

            return queryable;
        }
    }
}

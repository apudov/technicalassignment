﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using OrderApi.Models;

namespace OrderApi.Helpers
{
    public static class FillWithTestDataExtensions
    {
        private static readonly Address[] _addresses = new Address[]
        {
            GetAddress("Moscow", "Kremlin", "1", "103132", "+7 495 695-41-46"),
            GetAddress("Saint Petersburg", "Dvortsovaya Naberezhnaya", "32", "190000", "+7 812 710-90-79"),
            GetAddress("Moscow", "Theatre Square", "1", "125009", "+7 495 455-5555"),
            GetAddress("Saint Petersburg", "Theatre Square", "1", "190000", "+7 812 326-41-41"),
            GetAddress("Moscow", "Lavrushinsky Ln", "10", "119017", "+7 495 957-07-01")
        };

        private static readonly Customer[] _customers = new Customer[]
        {
            GetCustomer("Ansel", string.Empty, "Adams"),
            GetCustomer("Yousuf", string.Empty, "Karsh"),
            GetCustomer("Robert", string.Empty, "Capa"),
            GetCustomer("Henri", string.Empty, "Cartier-Bresson"),
            GetCustomer("Dorothea", string.Empty, "Lange"),
            GetCustomer("Jerry", string.Empty, "Uelsman"),
            GetCustomer("Annie", string.Empty, "Leibovitz"),
            GetCustomer("Brassaï", string.Empty, string.Empty),
            GetCustomer("Brian", string.Empty, "Duffy"),
            GetCustomer("Jay", string.Empty, "Maisel"),
            GetCustomer("Andrey", "Semenovich", "Pudov")
        };

        private static readonly int NumberOfOrders = 10;

        public static void FillWithTestData(this OrderContext context)
        {
            context.GeoNames.FillWithTestData(context);
            context.Names.FillWithTestData(context);
            context.Addresses.FillWithTestData(context, context.GeoNames);
            context.Customers.FillWithTestData(context, context.Names);
            context.Orders.FillWithTestData(context, context.Customers, context.Addresses);
            context.SaveChanges();
        }

        public static void FillWithTestData(this DbSet<GeoNameEntry> geoNames, OrderContext context)
        {
            foreach (var address in _addresses)
            {
                var a = geoNames.SingleOrDefaultAsync(geoName => geoName.Value == address.City.Value);

                if (geoNames.Any(geoName => geoName.Value == address.City.Value) == false)
                {
                    geoNames.Add(address.City);
                    context.SaveChanges();
                }

                if (geoNames.Any(geoName => geoName.Value == address.Street.Value) == false)
                {
                    geoNames.Add(address.Street);
                    context.SaveChanges();
                }
            }
        }

        public static void FillWithTestData(this DbSet<NameEntry> names, OrderContext context)
        {
            foreach (var customer in _customers)
            {
                if (names.Any(name => name.Value == customer.FirstName.Value) == false)
                {
                    names.Add(customer.FirstName);
                    context.SaveChanges();
                }

                if (names.Any(name => name.Value == customer.MiddleName.Value) == false)
                {
                    names.Add(customer.MiddleName);
                    context.SaveChanges();
                }

                if (names.Any(name => name.Value == customer.LastName.Value) == false)
                {
                    names.Add(customer.LastName);
                    context.SaveChanges();
                }
            }
        }

        public static void FillWithTestData(this DbSet<Address> addresses, OrderContext context, DbSet<GeoNameEntry> geoNames)
        {
            foreach (var address in _addresses)
            {
                var city = geoNames.FirstOrDefault(geoName => geoName.Value == address.City.Value);

                addresses.Add(new Address()
                {
                    City = geoNames.Single(geoName => geoName.Value == address.City.Value),
                    Street = geoNames.Single(geoName => geoName.Value == address.Street.Value),
                    Building = address.Building,
                    Postcode = address.Postcode,
                    PhoneNumber = address.PhoneNumber
                });

                context.SaveChanges();
            }
        }

        public static void FillWithTestData(this DbSet<Customer> customers, OrderContext context, DbSet<NameEntry> names)
        {
            foreach (var customer in _customers)
            {
                customers.Add(new Customer()
                {
                    FirstName = names.Single(name => name.Value == customer.FirstName.Value),
                    MiddleName = names.Single(name => name.Value == customer.MiddleName.Value),
                    LastName = names.Single(name => name.Value == customer.LastName.Value)
                });

                context.SaveChanges();
            }
        }

        public static void FillWithTestData(this DbSet<Order> orderItems, OrderContext context, DbSet<Customer> customers, DbSet<Address> addresses)
        {
            var generator = new Random(DateTime.Now.Millisecond);

            foreach (var index in Enumerable.Range(1, NumberOfOrders))
            {
                var ci = generator.Next(1, _customers.Length);
                var ai = generator.Next(1, _addresses.Length);

                orderItems.Add(new Order
                {
                    Price = generator.NextDouble() * 100,
                    Customer = customers.Single(customer => customer.Id == ci),
                    Address = addresses.Single(address => address.Id == ai)
                });

                context.SaveChanges();
            }
        }

        private static Address GetAddress(string city, string street, string building, string postcode, string phoneNumber)
        {
            return new Address()
            {
                City = new GeoNameEntry() { Value = city },
                Street = new GeoNameEntry() { Value = street },
                Building = building,
                Postcode = postcode,
                PhoneNumber = phoneNumber
            };
        }

        private static Customer GetCustomer(string firstName, string middleName, string lastName)
        {
            return new Customer()
            {
                FirstName = new NameEntry() { Value = firstName },
                MiddleName = new NameEntry() { Value = middleName },
                LastName = new NameEntry() { Value = lastName }
            };
        }
    }
}

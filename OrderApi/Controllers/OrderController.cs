﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OrderApi.Models;
using OrderApi.Helpers;

namespace OrderApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly OrderContext _context;

        public OrderController(OrderContext context)
        {
            _context = context;

            if (_context.Orders.Any() == false)
            {
                _context.FillWithTestData();
            }
        }

        // GET api/orders
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Order>>> GetOrders()
        {
            return await _context.Orders
                .Include(order => order.Customer).ThenInclude(customer => customer.FirstName)
                .Include(order => order.Customer).ThenInclude(customer => customer.MiddleName)
                .Include(order => order.Customer).ThenInclude(customer => customer.LastName)
                .Include(order => order.Address).ThenInclude(address => address.City)
                .Include(order => order.Address).ThenInclude(address => address.Street)
                .ToListAsync();
        }

        // GET api/order/{id}
        [HttpGet("{id}")]
        public async Task<ActionResult<Order>> GetOrder(int id)
        {
            var orderItem = await _context.Orders
                .Include(order => order.Customer).ThenInclude(customer => customer.FirstName)
                .Include(order => order.Customer).ThenInclude(customer => customer.MiddleName)
                .Include(order => order.Customer).ThenInclude(customer => customer.LastName)
                .Include(order => order.Address).ThenInclude(address => address.City)
                .Include(order => order.Address).ThenInclude(address => address.Street)
                .SingleOrDefaultAsync(order => order.Id == id); 

            return (orderItem == null) 
                ? (ActionResult<Order>) NotFound()
                : (ActionResult<Order>) orderItem;
        }

        // PUT api/order/{id}
        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateOrder(int id, [FromBody] Order updatedOrder)
        {
            var orderItem = await GetOrder(id);
            if (orderItem.Value != null)
            {
                var order = orderItem.Value;
                if (order.Id != updatedOrder.Id)
                {
                    return BadRequest();
                }

                orderItem.Value.Address = GetAddress(updatedOrder.Address);
                _context.SaveChanges();

                return Ok();
            }

            return NotFound();
        }

        // DELETE api/order/{id}
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteOrder(int id)
        {
            var order = await GetOrder(id);
            if (order.Value != null)
            {
                _context.Orders.Remove(order.Value);
                _context.SaveChanges();

                return Ok();
            }

            return NotFound();
        }

        private Address GetAddress(Address address)
        {
            GeoNameEntry GetGeoNameEntry(GeoNameEntry name)
            {
                var geoNameEntry = _context.GeoNames.SingleOrDefault(n => n.Value == name.Value);
                if (geoNameEntry == null)
                {
                    name.Id = 0;

                    _context.GeoNames.Add(name);
                    _context.SaveChanges();

                    return name;
                }

                return geoNameEntry;
            }

            var addressEntry = _context.Addresses.SingleOrDefault(a => a.City.Value == address.City.Value
               && a.Street.Value == address.Street.Value
               && a.Building == address.Building
               && a.Postcode == address.Postcode
               && a.PhoneNumber == address.PhoneNumber);
            if (addressEntry == null)
            {
                address.Id = 0;
                address.City = GetGeoNameEntry(address.City);
                address.Street = GetGeoNameEntry(address.Street);

                _context.Addresses.Add(address);
                _context.SaveChanges();

                return address;
            }

            return addressEntry;
        }
    }
}

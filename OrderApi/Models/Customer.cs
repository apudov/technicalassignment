﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OrderApi.Models
{
    public class Customer
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public NameEntry FirstName { get; set; }

        [Required]
        public NameEntry MiddleName { get; set; }

        [Required]
        public NameEntry LastName { get; set; }
    }
}

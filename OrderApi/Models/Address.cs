﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OrderApi.Models
{
    public class Address
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public GeoNameEntry City { get; set; }

        [Required]
        public GeoNameEntry Street { get; set; }

        [Required]
        public string Building { get; set; }

        [Required]
        public string Postcode { get; set; }

        public string PhoneNumber { get; set; }
    }
}

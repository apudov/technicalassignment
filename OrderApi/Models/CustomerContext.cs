﻿using Microsoft.EntityFrameworkCore;

namespace OrderApi.Models
{
    public class CustomerContext : DbContext
    {
        public CustomerContext(DbContextOptions<OrderContext> options)
            : base(options)
        {
        }

        public DbSet<CustomerItem> CustomerItems { get; set; }
    }
}

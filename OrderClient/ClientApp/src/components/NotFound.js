import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';

const NotFound = props => (
  <div>
    <h1 className = 'display-1'>404</h1>
    <h1>Sorry, the page not found.</h1>

    <p>
      The link you followed probably broken, or the page has been removed.
    </p>

    <p>
      Return to the <Link to = '/'>homepage.</Link>
    </p>
  </div>
);

export default connect() (NotFound);

import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { actionCreators } from '../store/Orders';

class OrdersDetails extends Component {
  componentDidMount() {
    this.ensureDataFetched();
  }

  componentDidUpdate() {
    this.ensureDataFetched();
  }

  ensureDataFetched() {
    this.props.requestOrders();
  }

  validationStyle(status) {
    return (status == null) ? '' : (status) ? 'is-valid' : 'is-invalid';
  }

  render() {
    const order = this.props.orders.find(order => order.id === parseInt(this.props.match.params.id, 10));
    if (order == null) {
      return ([]);
    }

    return (
      <div>
        <h1>Details</h1>

        { ((this.props.error != null) && (this.props.error !== '')) &&
          <div className = 'alert alert-danger my-4'>
            <strong>Oh, snap!</strong> {this.props.error}
          </div>}

        <form>
          <div className = 'form-row'>
            <div className = 'form-group col'>
              <label htmlFor = 'first-name'>First name</label>
              <input type = 'text' readOnly className = 'form-control' id = 'first-name' value = {order.customer.firstName.value} />
            </div>
            <div className = 'form-group col'>
              <label htmlFor = 'middle-name'>Middle name</label>
              <input type = 'text' readOnly className = 'form-control' id = 'middle-name' value = {order.customer.middleName.value} />
            </div>
            <div className = 'form-group col'>
              <label htmlFor = 'last-name'>Last name</label>
              <input type = 'text' readOnly className = 'form-control' id = 'last-name' value = {order.customer.lastName.value} />
            </div>
          </div>

          <div className = 'form-group row'>
            <label htmlFor = 'order-id' className = 'col-sm-2 col-form-label'>Id</label>
            <div className = 'col-sm-10'>
              <input type = 'text' readOnly className = 'form-control-plaintext' id = 'order-id' value = {'SAL' + order.id.toString().padStart(9, '0')} />
            </div>
          </div>
          <div className = 'form-group row'>
            <label htmlFor = 'order-price' className = 'col-sm-2 col-form-label'>Price</label>
            <div className = 'col-sm-10'>
              <input type = 'text' readOnly className = 'form-control-plaintext' id = 'order-price' value =  {String.fromCharCode(8381) + order.price.toFixed(2)} />
            </div>
          </div>

          <div className = 'form-group row'>
            <label htmlFor = 'city' className = 'col-sm-2 col-form-label'>City</label>
            <div className = 'col-sm-10'>
              <input id = 'city' type = 'text'
                className = {`form-control ${this.validationStyle(order.address.city.valid)}`}
                value = {order.address.city.value}
                onChange = {(event) => this.props.setCity(order.id, event.target.value)} />
            </div>
          </div>
          <div className = 'form-group row'>
            <label htmlFor = 'street' className = 'col-sm-2 col-form-label'>Street</label>
            <div className = 'col-sm-10'>
              <input id = 'street' type = 'text'
                className = {`form-control ${this.validationStyle(order.address.street.valid)}`}
                value = {order.address.street.value}
                onChange = {(event) => this.props.setStreet(order.id, event.target.value)} />
            </div>
          </div>
          <div className = 'form-group row'>
            <label htmlFor = 'building' className = 'col-sm-2 col-form-label'>Building</label>
            <div className = 'col-sm-10'>
              <input id = 'building' type = 'text'
                className = {`form-control ${this.validationStyle(order.address.buildingValid)}`}
                value = {order.address.building}
                onChange = {(event) => this.props.setBuilding(order.id, event.target.value)} />
            </div>
          </div>
          <div className = 'form-group row'>
            <label htmlFor = 'postcode' className = 'col-sm-2 col-form-label'>Postcode</label>
            <div className = 'col-sm-10'>
              <input id = 'building' type = 'text'
                className = {`form-control ${this.validationStyle(order.address.postcodeValid)}`}
                value = {order.address.postcode}
                onChange = {(event) => this.props.setPostcode(order.id, event.target.value)} />
            </div>
          </div>
          <div className = 'form-group row'>
            <label htmlFor = 'phone-number' className = 'col-sm-2 col-form-label'>Phone number</label>
            <div className = 'col-sm-10'>
              <input id = 'phone-number' type = 'text'
                className = {`form-control ${this.validationStyle(order.address.phoneNumberValid)}`}
                value = {order.address.phoneNumber}
                onChange = {(event) => this.props.setPhoneNumber(order.id, event.target.value)} />
            </div>
          </div>

          <Link className = 'btn btn-primary' to = {`/`}>Back</Link>
        </form>
      </div>
    );
  }
}

export default connect(
  state => state.orders,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(OrdersDetails);

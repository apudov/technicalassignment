import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { actionCreators } from '../store/Orders';

class Orders extends Component {
  componentDidMount() {
    this.ensureDataFetched();
  }

  componentDidUpdate() {
    this.ensureDataFetched();
  }

  ensureDataFetched() {
    this.props.requestOrders();
  }

  render() {
    return (
      <div>
        <h1>Orders</h1>
        {renderOrdersTable(this.props)}

        <p className = 'clearfix text-center'>
          {this.props.isLoading ? <span>Loading...</span> : []}
        </p>
      </div>
    );
  }
}

function renderOrdersTable(props) {
  return (
    <table className='table table-hover'>
      <thead>
        <tr>
          <th>Id</th>
          <th>Price</th>
          <th>Customer</th>
          <th />
        </tr>
      </thead>
      <tbody>
        {props.orders.map(order =>
          <tr key = {order.id}>
            <td>{'SAL' + order.id.toString().padStart(9, '0')}</td>
            <td>&#x20bd;{order.price.toFixed(2)}</td>
            <td>{order.customer.firstName.value} {order.customer.middleName.value} {order.customer.lastName.value}</td>
            <td><Link className = 'btn btn-default' to = {`/${order.id}`}>Details</Link></td>
          </tr>
        )}
      </tbody>
    </table>
  );
}

export default connect(
  state => state.orders,
  dispatch => bindActionCreators(actionCreators, dispatch)
)(Orders);

import React from 'react';
import { connect } from 'react-redux';

const About = props => (
  <div>
    <h1>Full Stack Software Developer (CCTS) Technical Assignment</h1>

    <h2>Things we look for in this test</h2>

    <p>
        We expect the candidate to show us his/her skills in Design/Architecture, ability to build testable and maintainable software using industry best practices. Do not overcomplicate, no need to do authentication or extensive styling. A simple, elegant, maintainable and working solution is good enough.
    </p>

    <h2>Description</h2>

    <p>Create REST API that can create and serve orders:</p>
    <ul>
        <li>Order has: ID (like SAL100489284), price, customer name, address.</li>
    </ul>

    <p>Create a Back-office web page that allows:</p>
    <ul>
        <li>to see the list of orders</li>
        <li>
            possibility to view a chosen order and

            <ul>
                <li>see details of the order</li>
                <li>change its address</li>
                <li>with validation of postcode and house number</li>
            </ul>
        </li>
        <li>example – <a href = 'http://albelli.nl'>http://albelli.nl</a> checkout (doesn’t have to be identical)</li>
        <li>
            <a href = 'https://www.dropbox.com/s/y3ohr4iy0lp8ccs/Address_validation.mp4'>https://www.dropbox.com/s/y3ohr4iy0lp8ccs/Address_validation.mp4</a>
            <ul>
                <li>see all Jira tickets that mention the order (by ID) with a clickable link (that leads to the Jira ticket page itself for more detail).</li>
            </ul>
        </li>
        <li>Test Jira (public): <a href = 'https://albelli-test-cc.atlassian.net/projects/CC/board'>https://albelli-test-cc.atlassian.net/projects/CC/board</a></li>
    </ul>

    <h2>Persistence</h2>
    <p>You can use the database of your choice, like mssql, postgres, sqlite, mongodb etc.</p>

    <h2>Source Code</h2>
    <p>
        You should create a private Git repository on <a href = 'https://bitbucket.org/'>https://bitbucket.org/</a> and give the user <a href = 'https://bitbucket.org/albumprinter'>https://bitbucket.org/albumprinter</a> read access to the repository.
    </p>

    <h2>Tools and libraries</h2>
    <p>Candidate is free to use any additional third-party libraries and frameworks. For backend, .NET is preferable; for frontend – any modern UI framework: Angular, React, Vue etc</p>

    <h2>Additional information</h2>
    <p>If you have any questions, please reach out to our recruiters. Feel free to improve the application as you see fit.</p>
  </div>
);

export default connect() (About);

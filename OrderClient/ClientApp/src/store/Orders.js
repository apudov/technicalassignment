import { validators } from '../helpers/Validators';

const requestOrdersType      = 'REQUEST_ORDERS';
const receiveOrdersType      = 'RECEIVE_ORDERS';
const requestOrderChangeType = 'REQUEST_ORDER_CHANGE';
const receiveOrderChangeType = 'RECEIVE_ORDER_CHANGE';
const failedOrderChangeType  = 'FAILED_ORDER_CHANGE';

const setCityType        = 'SET_CITY';
const setStreetType      = 'SET_STREET';
const setBuildingType    = 'SET_BUILDING';
const setPostcodeType    = 'SET_POSTCODE';
const setPhoneNumberType = 'SET_PHONE_NUMBER';

const initialState = { orders: [], isLoading: false };

export const actionCreators = {
  requestOrders: () => async (dispatch, getState) => {
    if (getState().orders.orders.length > 0) {
        return;
    }

    dispatch({ type: requestOrdersType });

    const url = 'https://localhost:5001/api/order';
    const response = await fetch(url);
    const orders = await response.json();

    dispatch({ type: receiveOrdersType, orders });
  },
  requestOrderChange: (orderId) => async (dispatch, getState) => {
    dispatch({ type: requestOrderChangeType });

    const order = getState().orders.orders.find(order => order.id === orderId);
    if (validators.isOrderValid(order) === false) {
      return;
    }

    const url = `https://localhost:5001/api/order/${orderId}`;
    await fetch(url, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'},
      body: JSON.stringify(order)})
      .then(response => (response.ok)
        ? dispatch({ type: receiveOrderChangeType })
        : dispatch({ type: failedOrderChangeType, error: response.statusText }))
      .catch(error => dispatch({ type: failedOrderChangeType, error: error }));
  },

  setCity: (orderId, city) => async (dispatch, getState) => {
    dispatch({ type: setCityType, orderId: orderId, city: city });
    dispatch(actionCreators.requestOrderChange(orderId));
  },
  setStreet: (orderId, street) => async (dispatch, getState) => {
    dispatch({ type: setStreetType, orderId: orderId, street: street });
    dispatch(actionCreators.requestOrderChange(orderId));
  },
  setBuilding: (orderId, building) => async (dispatch, getState) => {
    dispatch({ type: setBuildingType, orderId: orderId, building: building });
    dispatch(actionCreators.requestOrderChange(orderId));
  },
  setPostcode: (orderId, postcode) => async (dispatch, getState) => {
    dispatch({ type: setPostcodeType, orderId: orderId, postcode: postcode });
    dispatch(actionCreators.requestOrderChange(orderId));
  },
  setPhoneNumber: (orderId, phoneNumber) => async (dispatch, getState) => {
    dispatch({ type: setPhoneNumberType, orderId: orderId, phoneNumber: phoneNumber });
    dispatch(actionCreators.requestOrderChange(orderId));
  }
};

export const reducer = (state, action) => {
  state = state || initialState;

  if (action.type === requestOrdersType) {
    return {
      ...state,
      isLoading: true
    };
  }

  if (action.type === receiveOrdersType) {
    return {
      ...state,
      orders: action.orders,
      isLoading: false
    };
  }

  if (action.type === requestOrderChangeType) {
    return {
      ...state,
      error: null,
      isLoading: true
    };
  }

  if (action.type === receiveOrderChangeType) {
    return {
      ...state,
      isLoading: false
    };
  }

  if (action.type === failedOrderChangeType) {
    return {
      ...state,
      error: action.error,
      isLoading: false
    };
  }

  if (action.type === setCityType) {
    return {
      ...state,
      orders: state.orders
        .map(order =>
          (order.id === action.orderId)
            ? {...order, address: {
                ...order.address,
                city: {
                  ...order.address.city,
                  value: action.city,
                  valid: validators.isCityValid(action.city)}}}
            : order)
    };
  }

  if (action.type === setStreetType) {
    return {
      ...state,
      orders: state.orders
        .map(order =>
          (order.id === action.orderId)
            ? {...order, address: {
                ...order.address,
                street: {
                  ...order.address.street,
                  value: action.street,
                  valid: validators.isStreetValid(action.street)}}}
            : order)
    };
  }

  if (action.type === setBuildingType) {
    return {
      ...state,
      orders: state.orders
        .map(order =>
          (order.id === action.orderId)
            ? {...order, address: {
                ...order.address,
                building: action.building,
                buildingValid: validators.isBuildingValid(action.building)}}
            : order)
    };
  }

  if (action.type === setPostcodeType) {
    return {
      ...state,
      orders: state.orders
        .map(order =>
          (order.id === action.orderId)
            ? {...order, address: {
                ...order.address,
                postcode: action.postcode,
                postcodeValid: validators.isPostcodeValid(action.postcode)}}
            : order)
    };
  }

  if (action.type === setPhoneNumberType) {
    return {
      ...state,
      orders: state.orders
        .map(order =>
          (order.id === action.orderId)
            ? {...order, address: {
                ...order.address,
                phoneNumber: action.phoneNumber,
                phoneNumberValid: validators.isPhoneNumberValid(action.phoneNumber)}}
            : order)
    };
  }

  return state;
};

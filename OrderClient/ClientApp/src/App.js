import React from 'react';
import { Switch, Route } from 'react-router';

import Layout from './components/Layout';
import About from './components/About';
import NotFound from './components/NotFound';
import OrderDetails from './components/OrderDetails';
import Orders from './components/Orders';

export default () => (
  <Layout>
    <Switch>
      <Route exact path = '/' component = {Orders} />
      <Route exact path = '/:id([0-9]{1,16})' component = {OrderDetails} />
      <Route exact path = '/about' component = {About} />
      <Route component = {NotFound} />
    </Switch>
  </Layout>
);

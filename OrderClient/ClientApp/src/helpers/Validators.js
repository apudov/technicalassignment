export const validators = {
    isCityValid:        (value) => (/^[a-z ,.'-]+$/i.test(value)),
    isStreetValid:      (value) => (/^[a-z ,.'-]+$/i.test(value)),
    isBuildingValid:    (value) => (/^[0-9]{1,5}$/i.test(value)),
    isPostcodeValid:    (value) => (/^[A-z0-9 -]+$/i.test(value)),
    isPhoneNumberValid: (value) => (/^[0-9 ()+-]+$/i.test(value)),

    isOrderValid: (order) => (validators.isCityValid(order.address.city.value)
        && validators.isStreetValid(order.address.street.value)
        && validators.isBuildingValid(order.address.building)
        && validators.isPostcodeValid(order.address.postcode)
        && validators.isPhoneNumberValid(order.address.phoneNumber))
};
# Full Stack Software Developer (CCTS) Technical Assignment

## Things we look for in this test

We expect the candidate to show us his/her skills in Design/Architecture, ability to build testable and maintainable software using industry best practices. Do not overcomplicate, no need to do authentication or extensive styling. A simple, elegant, maintainable and working solution is good enough.

## Description

Create REST API that can create and serve orders:
- Order has: ID (like SAL100489284), price, customer name, address.

Create a Back-office web page that allows:
- to see the list of orders
- possibility to view a chosen order and
  - see details of the order
  - change its address
  - with validation of postcode and house number
- example – http://albelli.nl checkout (doesn’t have to be identical)
- https://www.dropbox.com/s/y3ohr4iy0lp8ccs/Address_validation.mp4
  - see all Jira tickets that mention the order (by ID) with a clickable link (that leads to the Jira ticket page itself for more detail).
- Test Jira (public): https://albelli-test-cc.atlassian.net/projects/CC/board

## Persistence

You can use the database of your choice, like mssql, postgres, sqlite, mongodb etc.

## Source Code

You should create a private Git repository on https://bitbucket.org/ and give the user https://bitbucket.org/albumprinter read access to the repository.

## Tools and libraries

Candidate is free to use any additional third-party libraries and frameworks. For backend, .NET is preferable; for frontend – any modern UI framework: Angular, React, Vue etc

## Additional information

If you have any questions, please reach out to our recruiters. Feel free to improve the application as you see fit.